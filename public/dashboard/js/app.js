$(document).ready(function () {
    /**
     * Editor Configration
     */
    $('[data-editor="true"]').trumbowyg({
        btnsDef: {
            // Customizables dropdowns
            image: {
                dropdown: ['insertImage', 'upload', 'base64', 'noembed'],
                ico: 'insertImage'
            }
        },
        btns: [
            ['viewHTML'],
            ['undo', 'redo'],
            ['formatting'],
            ['strong', 'em', 'del', 'underline'],
            ['link'],
            ['image'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['foreColor', 'backColor'],
            ['preformatted'],
            ['horizontalRule'],
            ['fullscreen'],
            ['upload']
        ],
        plugins: {
            // Add imagur parameters to upload plugin
            upload: {
                serverPath: 'https://api.imgur.com/3/image',
                fileFieldName: 'image',
                headers: {
                    'Authorization': 'Client-ID 9e57cb1c4791cea'
                },
                urlPropertyName: 'data.link'
            }
        }
    });
});

$(document).ready(function () {
    var fileList = [];
    $('#uploader input[type=file]').change(function () {
        /**
         * Create FileList and append hidden input
         */
        var files = $(this).get(0).files;
        for (var i = 0; i < files.length; i++) {
            fileList.push(files[i]);
        }
        $('#files').get(0).files = FileListItem(fileList);

        // preview images        
        preview(files);
    });

    /**
     * Remove image
     */
    $(document).on('click', '.preview-images .image button.delete', function() {
        // get image
        var image = $(this).closest('.image');
        
        // remove from dom
        image.remove();

        // if selected title image
        if($(this).closest('.image').find('.title-image.active').length > 0) {
            $('input[name=title-image]').val($('.preview-images .image:eq(0)').attr('data-image'));
            $('.preview-images .image:eq(0) .title-image').addClass('active');
        }

        // if new image
        if($.isNumeric(image.attr('data-image'))) {
            // remove FileList in image
            for (const key in fileList) {
                if (fileList.hasOwnProperty(key) && key == image.attr('data-image')) {
                    fileList.splice(key, 1);
                }
            }

            // generate new FileList
            var newFileList = [];
            for (var val of fileList) {
                newFileList.push(val);
            }

            // set new FileList
            $('#files').get(0).files = FileListItem(newFileList);
            
            // re-generate data-image is numeric
            for (let i = 0; i < $('.preview-images .image').length; i++) {
                var image = $('.preview-images .image:eq(' + i + ')');
                if($.isNumeric(image.attr('data-image'))) {
                    image.attr('data-image', i);
                }
            }
        } else {
            $('.hidden-inputs').append('<input type="hidden" name="deleted-images[]" value="' + image.attr('data-image') + '" />');
        }
    });

    /**
     * Set title image
     */
    $(document).on('click', '.preview-images button.title-image', function() {
        var image = $(this).closest('.image');

        // set title image index
        $('input[name=title-image]').val(image.attr('data-image'));

        // remove all active class
        $('button.title-image').removeClass('active');

        // set this active class
        $(this).addClass('active');
    });
});

function preview(files) {
    if ( typeof preview.counter == 'undefined' ) {
        preview.counter = 0;
    }

    // reader file
    reader(files[preview.counter]).then(function(image) {
        /**
         * Set first active and title image
         */
        if($('.preview-images .image').length == 0 && preview.counter == 0) {
            var active = 'active';
            $('input[name=title-image]').val(0);
        } else {
            var active = '';
        }

        // append image from dom
        $('.preview-images').append('\
            <div class="image col-md-4 mt-5" data-image="' + $('.preview-images .image').length + '">\
                <img src="' + image + '" width="100%" height="170" alt="">\
                <div class="row mt-5">\
                    <div class="col-6">\
                        <button type="button" class="btn btn-primary w-100 title-image ' + active + '">\
                            <i class="fa fa-photo"></i>\
                        </button>\
                    </div>\
                    <div class="col-6">\
                        <button type="button" class="btn btn-primary w-100 delete">\
                            <i class="fa fa-trash"></i>\
                        </button>\
                    </div>\
                </div>\
            </div>\
        ');
        
        preview.counter++;
        if(files.length > preview.counter) {
            preview(files);
        } else {
            preview.counter = undefined;
        }
    });
}

function reader(file) {
    return new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = function(event) {
            resolve(event.target.result);
        }
        reader.onerror = reader.onabort = reject;
        reader.readAsDataURL(file);
    });
}

function FileListItem(a) {
    a = [].slice.call(Array.isArray(a) ? a : arguments)
    for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
    if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
    for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
    return b.files
}