<?php

    /*
    * File: index.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    require_once './vendor/autoload.php';

    // start session
    session_start();

    // get environment
    switch ($_SERVER["HTTP_HOST"]) {
        case "localhost":
            define('ENVIRONMENT', 'development');
            break;
        default:
            define('ENVIRONMENT', 'production');
            break;
    }

    // load .env file
    $dotenv = new Dotenv\Dotenv(__DIR__, ENVIRONMENT == 'production' ? '.env' : '.env.dev');
    $dotenv->load(true);

    // languages
    $allowedLocales = ['en', 'tr', 'ru'];
    $defaultLocale  = 'en';
    
    // instantiate the App object
    $app = new \Slim\App([
        'settings' => [
            'determineRouteBeforeAppMiddleware' => true,
            'displayErrorDetails' => filter_var($_ENV['APP_DEBUG'], FILTER_VALIDATE_BOOLEAN),
            'allowedLocales' => $allowedLocales,
            'defaultLocale'  => $defaultLocale,
            'db' => [
                'driver'    => 'mysql',
                'host'      => $_ENV['DB_HOST'],
                'database'  => $_ENV['DB_NAME'],
                'username'  => $_ENV['DB_USER'],
                'password'  => $_ENV['DB_PASS'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ]
        ]
    ]);
   
    // require middleware
    require_once 'app/middleware.php';

    // require containers
    require_once 'app/containers.php';
    
    // require routes
    require_once 'app/routes.php';

    // run application
    $app->run();

?>