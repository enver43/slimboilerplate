<?php

    /*
    * File: dashboard.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    use App\System\Helpers\Auth;
    use App\System\Helpers\Url;

    /**
     * Dashboard Middleware
     */
    $mw = function($request, $response, $next) {
        if(Auth::isAuth('admin')) {
            return $next($request, $response);
        } else if($request->getAttribute('route')->getName() != 'dashboard.index') {
            return Url::redirect('dashboard.index');
        }
        return $next($request, $response);
    };

    /**
     * Dashboard Routes
     */
    $app->group('/dashboard', function() {
        // index
        $this->map(['GET', 'POST'], '', '\App\Controllers\Dashboard\MainController:index')->setName('dashboard.index');
        
        /**
         * More Routes
         */
        $this->group('/more', function() {
            // settings
            $this->map(['GET', 'POST'], '/settings', '\App\Controllers\Dashboard\MoreController:settings')->setName('dashboard.more.settings');
        });

        /**
         * Translate Routes
         */
        $this->group('/translate', function() {
            // index
            $this->get('', '\App\Controllers\Dashboard\TranslateController:index')->setName('dashboard.translate.index');
            // edit
            $this->map(['GET', 'POST'], '/edit/{lang}', '\App\Controllers\Dashboard\TranslateController:edit')->setName('dashboard.translate.edit');
        });

        // logout
        $this->get('/logout', '\App\Controllers\Dashboard\MainController:logout')->setName('dashboard.logout');
    })->add($mw);

?>