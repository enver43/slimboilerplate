<?php

    /*
    * File: site.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    /**
     * Site Routes
     */
    $routes = function($app) {
        $app->get('', '\App\Controllers\Site\MainController:index')->setName('index');
    };

    /**
     * Configration Routes
     */
    $app->group('/{locale:[a-z]{2}}', function() use ($routes) {
        $routes($this);
    });
    $app->group('/', function() use ($routes) {
        $routes($this);
    });

?>