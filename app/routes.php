<?php

    /*
    * File: routes.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    require_once 'routes/site.php';
    require_once 'routes/dashboard.php';
    require_once 'routes/api.php';
    require_once 'routes/crons.php';
    
?>