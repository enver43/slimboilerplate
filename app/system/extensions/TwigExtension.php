<?php

    /*
    * File: TwigExtension.php
    * File Created: Saturday, 30th March 2019 11:38:16 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\System\Extensions;

    use Twig_Extension;
    use Twig_SimpleFunction;
    use Twig_Filter;
    use App\System\Helpers\Main;
    use App\System\Helpers\Auth;
    use App\System\Helpers\Date;
    use App\System\Helpers\Url;
    use App\System\Helpers\Str;

    class TwigExtension extends Twig_Extension
    {
        protected $container;

        public function __construct($container)
        {
            $this->container = $container;
        }

        /**
         * Get functions
         *
         * @return array
         */
        public function getFunctions()
        {
            return [
                new Twig_SimpleFunction('__', [$this, 'trans']),
                new Twig_SimpleFunction('getEnv', [$this, 'getEnv']),
                new Twig_SimpleFunction('isPost', [$this, 'isPost']),
                new Twig_SimpleFunction('getLocale', [$this, 'getLocale']),
                new Twig_SimpleFunction('timeAgo', [$this, 'timeAgo']),
                new Twig_SimpleFunction('isAuth', [$this, 'isAuth']),
                new Twig_SimpleFunction('inJson', [$this, 'inJson']),
                new Twig_SimpleFunction('storagePath', [$this, 'storagePath']),
                new Twig_SimpleFunction('publicPath', [$this, 'publicPath']),
                new Twig_SimpleFunction('getLocaleUrl', [$this, 'getLocaleUrl']),
                new Twig_SimpleFunction('permalink', [$this, 'permalink'])
            ];
        }

        /**
         * Get filters
         *
         * @return array
         */
        public function getFilters()
        {
            return [
                new Twig_Filter('json_decode', [$this, 'jsonDecode']),
                new Twig_Filter('strftime', [$this, 'strfTime'])
            ];
        }

        /**
         * Get json decoded array
         *
         * @param string $json
         * @return array
         */
        public function jsonDecode($json, $assoc = true)
        {
            return json_decode($json, $assoc);
        }
        
        /**
         * Set date format locale
         *
         * @param integer $timestamp
         * @param string $format
         * @return string
         */
        public function strfTime($timestamp, $format)
        {
            return strftime($format, $timestamp);
        }

        /**
         * Exist In Json
         *
         * @param json $json
         * @param string $search
         * @return boolean
         */
        public function inJson($json, $search)
        {
            $json = json_decode($json, true);
            if(in_array($search, $json)) {
                return true;
            }
            return false;
        }

        /**
         * Get translation value
         *
         * @param string $key
         * @param array $replace
         * @return string
         */
        public function trans($key, $replace = [])
        {
            return $this->container->translator->trans($key, $replace);
        }

        /**
         * Get current language
         *
         * @return string
         */
        public function getLocale()
        {
            return Main::getLocale();
        }

        /**
         * Get environment
         *
         * @param string $name
         * @return void
         */
        public function getEnv($name)
        {
            return $_ENV[$name];
        }
        
        /**
         * Check request POST
         *
         * @return boolean
         */
        public function isPost()
        {
            if(Main::getServer('REQUEST_METHOD') == 'POST') {
                return true;
            }
            return false;
        }

        /**
         * Check auth
         *
         * @param string $role
         * @return boolean
         */
        public function isAuth($role = 'user')
        {
            return Auth::isAuth($role);
        }

        /**
         * Get time ago
         *
         * @param integer|string $datetime
         * @param boolean $full
         * @return string
         */
        public function timeAgo($datetime, $full = false)
        {
            return Date::timeAgo($datetime, $full);
        }
        
        /**
         * Get Storage Path
         *
         * @param string $file
         * @param string $path
         * @return string
         */
        public function storagePath($path)
        {
            return Url::baseUrl('storage/' . $path);
        }

        /**
         * Get Public Path
         *
         * @param string $file
         * @param string $path
         * @return string
         */
        public function publicPath($path = '')
        {
            return Url::baseUrl('public/' . $path);
        }

        /**
         * Get Locale Full Url
         *
         * @param string $locale
         * @return string
         */
        public function getLocaleUrl($locale)
        {
            $path = $this->container->get('request')->getUri()->getPath();
            $path = $path[0] == '/' ? substr($path, 1) : $path;
            $path = preg_replace("/(^[a-z]{2}$)|(^[a-z]{2}\/)/", '', $path);
            return Url::baseUrl($locale . '/' . $path);
        }

        /**
         * Generate Permalink
         *
         * @param string $string
         * @return string
         */
        public function permalink($string)
        {
            return Str::permalink($string);
        }
    }

?>