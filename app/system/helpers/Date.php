<?php

    /*
    * File: Date.php
    * File Created: Saturday, 30th March 2019 11:38:16 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\System\Helpers;

    class Date
    {
        /**
         * Date Time Ago
         *
         * @param string|integer $datetime
         * @param boolean $full
         * @return string
         */
        public static function timeAgo($datetime, $full = false)
        {
            $now = new \DateTime;
            $ago = new \DateTime((is_numeric($datetime) ? '@' . $datetime : $datetime));
            $diff = $now->diff($ago);
        
            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;
        
            $string = [
                'y' => $GLOBALS['container']->translator->trans('main.year'),
                'm' => $GLOBALS['container']->translator->trans('main.month'),
                'w' => $GLOBALS['container']->translator->trans('main.week'),
                'd' => $GLOBALS['container']->translator->trans('main.day'),
                'h' => $GLOBALS['container']->translator->trans('main.hour'),
                'i' => $GLOBALS['container']->translator->trans('main.minute'),
                's' => $GLOBALS['container']->translator->trans('main.second')
            ];
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v;
                } else {
                    unset($string[$k]);
                }
            }
        
            if (!$full) $string = array_slice($string, 0, 1);
            return $string ? implode(', ', $string) . ' ' . $GLOBALS['container']->translator->trans('main.ago') : $GLOBALS['container']->translator->trans('main.just_now');
        }
    }

?>