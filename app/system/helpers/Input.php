<?php

    /*
    * File: Input.php
    * File Created: Saturday, 30th March 2019 11:38:16 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\System\Helpers;

    Class Input
    {
        private static $inputValid = [];

        /**
         * Get
         *
         * @param string $key
         * @param boolean $striptTags
         * @return void
         */
        public static function get($key, $striptTags = true, $allowedTags = null)
        {
            $get = $GLOBALS['container']->get('request')->getQueryParams();
            if(!empty($get) && isset($get[$key])) {
                return self::clean($get[$key], $striptTags, $allowedTags);
            }
            return false;
        }

        /**
         * Post
         *
         * @param string $key
         * @param boolean $striptTags
         * @return void
         */
        public static function post($key, $striptTags = true, $allowedTags = null)
        {
            $post = $GLOBALS['container']->get('request')->getParsedBody();
            if(!empty($post) && isset($post[$key])) {
                return self::clean($post[$key], $striptTags, $allowedTags);
            }
            return false;
        }

        /**
         * Clean
         *
         * @param string $value
         * @param boolean $striptTags
         * @return void
         */
        private static function clean($value, $striptTags, $allowedTags)
        {
            if($striptTags === true) {
                return $allowedTags !== null ? strip_tags($value, $allowedTags) : strip_tags($value);
            }
            $value = htmlspecialchars($value, ENT_QUOTES);
            $value = trim($value);
            return $value;
        }
        
        /**
         * Validate Input
         *
         * @param array $inputs
         * @return void
         */
        // public static function validateInput($inputs)
        // {
        //     // default configration
        //     $defaultConfig = [
        //         'minLength'   => null,
        //         'maxLength'   => null,
        //         'notBlank'    => false,
        //         'isEmail'     => false,
        //         'striptTags'  => true,
        //         'allowedTags' => null,
        //         'regex'       => null
        //     ];

        //     $defaultMessages = [
        //         'lengthMessage' => '' 
        //     ];

        //     self::$inputValid = [];
        //     foreach ($inputs as $input => $config) {
        //         // merge input config and default config
        //         if(is_array($config)) {
        //             $config = array_merge($defaultConfig, $config);
        //         } else {
        //             $config = $defaultConfig;
        //         }

        //         // input error status
        //         $inputError = false;

        //         // get input value
        //         $inputVal = self::post($input) ?? self::get($input);
                
        //         // if input is empty
        //         if($config['notBlank'] === true) {
        //             // clean input
        //             $clean = self::clean($inputVal, $config['striptTags'] ?? false, $config['allowedTags'] ?? null);
        //             if(!$clean || $clean == '') {
        //                 $inputError = true;
        //             }
        //         }

        //         // if input is email
        //         if($config['isEmail'] === true) {
        //             // if input email not valid
        //             if(!Email::valid($inputVal)) {
        //                 $inputError = true;
        //             }
        //         }

        //         // if input regex
        //         if($config['regex'] !== null) {
        //             if(!preg_match($config['regex'], $inputVal)) {
        //                 $inputError = true;
        //             }
        //         }

        //         // check input length
        //         if($config['minLength'] !== null || $config['maxLength'] !== null) {
        //             if(strlen($inputVal) < $config['minLength']) {
        //                 $inputError = true;
        //             }

        //             if(strlen($inputVal) > $config['maxLength']) {
        //                 $inputError = true;
        //             }
        //         }

        //         // set input valid status
        //         self::$inputValid[$input] = $inputError;
        //     }
        //     return new self;
        // }

        // /**
        //  * Check all validation error
        //  *
        //  * @return boolean
        //  */
        // public function hasErrors()
        // {
        //     return in_array(true, array_values(self::$inputValid));
        // }

        // /**
        //  * Check input validation error
        //  *
        //  * @return boolean
        //  */
        // public function hasInputError($key)
        // {
        //     if(isset(self::$inputValid[$key])) {
        //         return self::$inputValid[$key];
        //     }
        //     return false;
        // }
    }

?>