<?php

    /*
    * File: Auth.php
    * File Created: Saturday, 30th March 2019 11:38:16 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\System\Helpers;

    class Auth
    {
        /**
         * Get Authencation
         *
         * @param string $role
         * @return boolean
         */
        public static function isAuth($role = 'user')
        {
            if(Session::get($role . '_login')) {
                return true;
            }
            return false;
        }

        /**
         * Set Authencation
         *
         * @param string $role
         * @return boolean
         */
        public static function setAuth($role = 'user')
        {
            if(Session::set($role . '_login', true)) {
                return true;
            }
            return false;
        }

        /**
         * Destroy Authencation
         *
         * @param string $role
         * @return boolean
         */
        public static function destroyAuth($role = 'user')
        {
            if(Session::destroy($role . '_login')) {
                return true;
            }
            return false;
        }
    }

?>