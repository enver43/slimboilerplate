<?php

	/*
	* File: Email.php
	* File Created: Saturday, 30th March 2019 11:38:16 am
	* Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
	*/

	namespace App\System\Helpers;

	class Email
	{
		/**
		 * Validate Email
		 *
		 * @param string $email
		 * @return boolean
		 */
		public static function valid($email)
		{
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				return true;
			}
			return false;
		}

		/**
		 * Send Email
		 *
		 * @param string $email
		 * @param string $subject
		 * @param string $body
		 * @return array
		 */
		public static function send($email, $subject, $body)
		{
			// smtp settings
			$smtpSetting = [
				'host'   => $_ENV['SMTP_HOST'],
				'port'   => $_ENV['SMTP_PORT'],
				'secure' => $_ENV['SMTP_SECURE'],
				'email'  => $_ENV['SMTP_USER'],
				'pass'   => $_ENV['SMTP_PASS']
			];
			
			$mail = new \PHPMailer;
			$mail->setLanguage(Main::getLocale());
			$mail->SMTPDebug = 2;
			$mail->isSMTP();
			$mail->SMTPAuth = true;
			$mail->Host = $smtpSetting['host'];
			$mail->Username = $smtpSetting['email'];
			$mail->Password = $smtpSetting['pass'];
			$mail->SMTPSecure = $smtpSetting['secure'];
			$mail->Port = $smtpSetting['port'];

			$mail->setFrom($mail->Username, $_ENV['APP_NAME']);
			$mail->addAddress($email, $_ENV['APP_NAME']);

			$mail->isHTML(true);
			$mail->Subject = $subject;
			$mail->Body = $body;
			$mail->CharSet = 'UTF-8';

			$mail->SMTPOptions = [
				'ssl' => [
					'verify_peer'       => false,
					'verify_peer_name'  => false,
					'allow_self_signed' => true
				]
			];

			if (!$mail->Send()) {
				return [
					'status'  => false,
					'message' => $mail->ErrorInfo
				];
			}
			return [
				'status' => true
			];
		}
	}

?>
