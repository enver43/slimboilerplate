<?php

    /*
    * File: Url.php
    * File Created: Saturday, 30th March 2019 11:38:16 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\System\Helpers;

    class Url
    {
        /**
         * Redirect URL
         *
         * @param string $url
         * @return void
         */
        public static function redirect($routeName = null, $data = [])
        {
            $response = $GLOBALS['container']->get('response');
            return $response->withRedirect(self::pathFor($routeName ?? 'index', $data));
        }

        /**
         * Get Base URL
         *
         * @param string $path
         * @return string
         */
        public static function baseUrl($path = null)
        {
            return $_ENV['APP_URL'] . ($path !== null ? '/' . $path : '');
        }

        /**
         * Get URL for Route Name
         *
         * @param string $name
         * @param array $data
         * @param array $queryParams
         * @return string
         */
        public static function pathFor($name, $data = [], $queryParams = [])
        {
            return $GLOBALS['container']->get('router')->pathFor($name, $data, $queryParams);
        }
    }
    
?>