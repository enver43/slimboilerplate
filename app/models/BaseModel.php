<?php

    /*
    * File: BaseModel.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\Models;

    class BaseModel
    {
        /**
         * Get Container
         *
         * @param string $container
         * @return void
         */
        protected static function get($container)
        {
            return $GLOBALS['container']->get($container);
        }

        /**
         * Get Current Locale
         *
         * @return string
         */
        public function getLocale()
        {
            return self::get('translator')->trans('main.locale');
        }

        /**
         * Get Count
         *
         * @param string $table
         * @param array|string $where
         * @return integer
         */
        public static function count($table, $where = null)
        {
            $totalItems = self::get('db')->table($table);
            if($where !== null) {
                $totalItems->where($where);
            }
            return $totalItems->count('id');
        }

        /**
         * Get Sum Column
         *
         * @param string $table
         * @param string $column
         * @param array|string $where
         * @return integer
         */
        public static function sum($table, $column, $where = null)
        {
            $totalItems = self::get('db')->table($table);
            if($where !== null) {
                $totalItems->where($where);
            }
            return $totalItems->sum($column);
        }
    }

?>