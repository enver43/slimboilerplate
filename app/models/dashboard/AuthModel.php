<?php

    /*
    * File: AuthModel.php
    * File Created: Saturday, 30th March 2019 12:19:19 pm
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\Models\Dashboard;

    use App\Models\BaseModel;

    class AuthModel extends BaseModel
    {
        /**
         * Get Information
         *
         * @param string $username
         * @param string $pass
         * @return boolean
         */
        public static function info()
        {
            $result = self::get('db')->table('dashboard')
                ->first();
            
            if(!empty($result)) {
                return $result;
            }
            return false;
        }

        /**
         * Login
         *
         * @param string $username
         * @param string $pass
         * @return boolean
         */
        public static function login($username, $pass)
        {
            $result = self::get('db')->table('dashboard')
                ->where([
                    ['username', '=', $username],
                    ['pass', '=', $pass]
                ])
                ->first();
            
            if(!empty($result)) {
                return true;
            }
            return false;
        }

        /**
         * Update
         * 
         * @param array $data
         * @return boolean
         */
        public static function update($data)
        {
            return self::get('db')->table('dashboard')
                ->update($data);
        }
    }

?>