<?php

    return [
        "locale" => "en",
        "page_not_found" => "Page not found",
        "not_found" => "No results found",
        "year" => "year",
        "month" => "month",
        "week" => "week",
        "day" => "day",
        "hour" => "hour",
        "minute" => "minute",
        "second" => "second",
        "ago" => "ago",
        "just_now" => "just now",
        "ok" => "Ok",
        "next" => "Next",
        "prev" => "Prev",
        "go_back" => "Go back"
    ];

?>