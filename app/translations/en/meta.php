<?php

    return [
        "site_name" => "Slim Boilerplate",
        "keywords" => "CNN,CNN news,CNN.com,CNN TV,news,news online,breaking news,U.S. news,world news,weather,business,CNN money,sports,politics,law,technology, entertainment,education,travel,health,special reports,autos,developing story,news video,CNN Intl",
        "description" => "The fastest, easiest way to build a great looking mobile website in minutes. Includes hosting, site analytics, click-to-call, mobile maps and more"
    ];

?>