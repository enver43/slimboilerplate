<?php 

	return [
		"locale" => "tr",
		"page_not_found" => "Sayfa bulunamadı",
		"not_found" => "Sonuç bulunamadı",
		"year" => "yıl",
		"month" => "ay",
		"week" => "hafta",
		"day" => "gün",
		"hour" => "saat",
		"minute" => "dakika",
		"second" => "saniye",
		"ago" => "önce",
		"just_now" => "şimdi",
		"ok" => "Tamam",
		"next" => "İleri",
		"prev" => "Geri",
		"go_back" => "Geri dön"
	]; 

?>