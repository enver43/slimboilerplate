<?php

    /*
    * File: middleware.php
    * File Created: Tuesday, 9th April 2019 11:29:24 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    // Last trailing slash middleware
    $app->add(function ($request, $response, $next) {
        $uri = $request->getUri();
        $path = $uri->getPath();
        if ($path != '/' && substr($path, -1) == '/') {
            // permanently redirect paths with a trailing slash
            // to their non-trailing counterpart
            $uri = $uri->withPath(substr($path, 0, -1));
            
            if($request->getMethod() == 'GET') {
                return $response->withRedirect((string)$uri, 301);
            } else {
                return $next($request->withUri($uri), $response);
            }
        }
        return $next($request, $response);
    });

    // Add locale middleware
    $app->add(new App\System\Middleware\Locale([
        'allowedLocales' => $allowedLocales,
        'defaultLocale' => $defaultLocale
    ]));

    // Add middleware csrf protection 
    $app->add(function ($request, $response, $next) {
        $csrf = $this->get('csrf');
        $csrf->protectJqueryAjax(false);
        $csrf->protectForms(true, false);
        $csrf->setSalt($_ENV['APP_SECRET']);
        return $csrf->__invoke($request, $response, $next);
    });

?>