<?php
    
    /*
    * File: BaseController.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\Controllers;

    use \Psr\Container\ContainerInterface;

    class BaseController
    {
        protected $container;
        
        public function __construct(ContainerInterface $container)
        {
            $this->container = $container;
        }

        /**
         * Get Current Locale
         *
         * @return string
         */
        public function getLocale()
        {
            return $this->trans('main.locale');
        }

        /**
         * Get translation value
         *
         * @param string $key
         * @param array $replace
         * @return string
         */
        public function trans($key, $replace = [])
        {
            return $this->container->translator->trans($key, $replace);
        }
        
        /**
         * Get Property
         *
         * @param object $property
         * @return void
         */
        public function __get($property)
        {
            if (isset($this->container->{$property})) {
                return $this->container->{$property};
            }
        }
    }

?>