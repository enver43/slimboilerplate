<?php

    /*
    * File: MainController.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */
    
    namespace App\Controllers\Site;

    use App\Controllers\BaseController;

    class MainController extends BaseController
    {
        public function index($request, $response, $args)
        {
            // $meta = [
            //     'title' => 'Example',
            //     'images' => [
            //         'news/thumb/12456775.jpg',
            //         'news/thumb/10024558.jpg'
            //     ],
            //     'keywords' => 'keyword1, keyword2',
            //     'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis, numquam corrupti quod possimus beatae accusamus laborum aperiam ducimus? Laudantium reprehenderit harum incidunt recusandae nostrum quaerat enim rerum pariatur deleniti eligendi?'
            // ];

            return $this->view->render($response, 'site/index.html');
        }
    }

?>