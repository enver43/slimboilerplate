<?php

    /*
    * File: TranslateController.php
    * File Created: Saturday, 30th March 2019 2:20:05 pm
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\Controllers\Dashboard;

    use App\Controllers\BaseController;
    use App\System\Helpers\Url;

    class TranslateController extends BaseController
    {
        public function index($request, $response, $args)
        {
            $langs = [];
            foreach (glob('app/translations/*', GLOB_ONLYDIR) as $folder) {
                $folder  = explode('/', $folder);
                $langs[] = end($folder);
            }
            return $this->view->render($response, 'dashboard/translate/index.html', [
                'langs' => $langs
            ]);
        }

        public function edit($request, $response, $args)
        {
            $lang = $args['lang'];

            if(is_dir('app/translations/' . $lang)) {
                if($request->isPost()) {
                    $body = $request->getParsedBody();

                    $error = false;

                    foreach ($body['file'] as $file) {
                        
                        $fileBody = [];
                        if($file == 'main') {
                            $fileBody['locale'] = $lang;
                        }
                        $fileBody = array_merge($fileBody, $body[$file]);
                        
                        $fileContent = "<?php \n\n\treturn [\n";
                            
                            $i = 1;
                            foreach ($fileBody as $key => $value) {
                                $value = str_replace('"', "'", $value);
                                $fileContent .= "\t\t" . '"' . $key . '" => "' . $value . '"' . (count($fileBody) > $i ? ',' . PHP_EOL : '');
                                $i++;
                            }

                        $fileContent .= "\n\t]; \n\n?>";
                        
                        $put = file_put_contents('app/translations/' . $lang . '/' . $file . '.php', $fileContent);
                        if(!$put) {
                            $error = true;
                        }
                    }
                    
                    if(!$error) {
                        $this->flash->addMessage('success', 'Tərcümələr yeniləndi');
                    } else {
                        $this->flash->addMessage('danger', 'Bir xəta baş verdi. Zəhmət olmasa bir az sonra yenidən cəhd edin');
                    }
                    
                    return Url::redirect('dashboard.translate.edit', ['lang' => $lang]);
                }

                $langFiles = [];
                foreach (glob('app/translations/' . $lang . '/*.php') as $file) {
                    $output = include($file);
                    $filename = basename($file);
                    $langFiles[$filename] = $output;
                }

                return $this->view->render($response, 'dashboard/translate/edit.html', [
                    'flash'     => $this->flash->getMessages(),
                    'lang'      => $lang,
                    'langFiles' => $langFiles
                ]);
            }
            return Url::redirect('dashboard.translate.index');
        }
    }

?>