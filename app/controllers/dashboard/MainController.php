<?php

    /*
    * File: MainController.php
    * File Created: Saturday, 30th March 2019 12:17:40 pm
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\Controllers\Dashboard;

    use App\Controllers\BaseController;
    use App\Models\Dashboard\AuthModel;
    use App\System\Helpers\Auth;
    use App\System\Helpers\Url;

    class MainController extends BaseController
    {
        public function index($request, $response)
        {
            if($request->isPost()) {
                $body = $request->getParsedBody();

                if($body['username'] != '' && $body['pass'] != '') {
                    $login = AuthModel::login($body['username'], md5($body['pass']));
                    if($login === true) {
                        Auth::setAuth('admin');
                    }
                }
                return Url::redirect('dashboard.index');
            }
            return $this->view->render($response, 'dashboard/index.html');
        }

        public function logout()
        {
            Auth::destroyAuth('admin');
            return Url::redirect('dashboard.index');
        }
    }

?>