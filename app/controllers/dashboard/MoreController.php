<?php

    /*
    * File: MoreController.php
    * File Created: Tuesday, 9th April 2019 8:04:17 pm
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    namespace App\Controllers\Dashboard;

    use App\Controllers\BaseController;
    use App\Models\Dashboard\AuthModel;
    use App\System\Helpers\Url;

    class MoreController extends BaseController
    {
        public function settings($request, $response)
        {
            if($request->isPost()) {
                $body = $request->getParsedBody();

                $username = strip_tags(trim($body['username']));
                $pass     = strip_tags(trim($body['pass']));

                // update data
                $updateData = [
                    'username' => $username
                ];

                if($pass != '') {
                    $updateData['pass'] = md5($pass);
                }

                if($username != '') {
                    try {
                        AuthModel::update($updateData);
                        // add flash message
                        $this->flash->addMessage('success', 'Məlumatlar yeniləndi');
                    } catch (\Illuminate\Database\QueryException $e) {
                        // add flash message
                        $this->flash->addMessage('danger', 'Database Error: ' . $e->getMessage());
                    }
                }
                return Url::redirect('dashboard.more.settings');
            }
            return $this->view->render($response, 'dashboard/more/settings.html', [
                'flash' => $this->flash->getMessages(),
                'info' => AuthModel::info()
            ]);
        }
    }

?>