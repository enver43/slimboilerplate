<?php

    /*
    * File: containers.php
    * File Created: Saturday, 30th March 2019 11:38:15 am
    * Author: Anvar Abbasov (anvar.z.abbasov@gmail.com)
    */

    $container = $app->getContainer();

    // exception handler
    $container['errorHandler'] = function($container) {
        return function($request, $response, $exception) use($container) {
            $data = [
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine(),
                'trace'   => explode("\n", $exception->getTraceAsString()),
            ];

            $container->view->render($response, '_errors/all.html', [
                'error' => $data,
                'title' => 'Server Error'
            ]);
            return $response->withStatus(500);
        };
    };

    // page not found handler
    $container['notFoundHandler'] = function ($container) {
        return function ($request, $response) use($container) {
            $container->view->render($response, '_errors/404.html', [
                'error' => [],
                'title' => $container->get('translator')->trans('main.page_not_found')
            ]);
            return $response->withStatus(404);
        };
    };

    // database
    $container['db'] = function ($container) {
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection($container['settings']['db']);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        return $capsule->getConnection();
    };

    // csrf
    $container['csrf'] = function ($container) {
        // get the current session id
        $sessionId = session_id();
        return new \Odan\Slim\Csrf\CsrfMiddleware($sessionId);
    };

    // flash message
    $container['flash'] = function ($container) {
        return new \Slim\Flash\Messages();
    };

    // twig view
    $container['view'] = function ($container) {
        // set view directory
        $view = new \Slim\Views\Twig(__DIR__ . '/view');
        
        $router = $container->get('router');
        $uri    = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
        $view->addExtension(new \Twig_Extension_Debug());
        $view->addExtension(new \App\System\Extensions\TwigExtension($container));
        
        // Add a global twig variable csrf token
        $csrf = $container->get('csrf');
        $csrf->setSalt($_ENV['APP_SECRET']);
        $csrfToken = $csrf->getToken();
        $view->getEnvironment()->addGlobal('csrf_token', $csrfToken);

        // all view render
        $view['siteName'] = $container->get('translator')->trans('meta.site_name');
        $view['baseUrl']  = $_ENV['APP_URL'];
        $view['fullUrl']  = $container->get('request')->getUri();
        $view['locale']   = ($_SESSION['locale'] ?? $container->get('settings')['defaultLocale']);
        $view['session']  = $_SESSION;
        $view['cookie']   = $_COOKIE;
        $view['env']      = ENVIRONMENT;

        return $view;
    };

    // translator
    $container['translator'] = function($container) {
        $settings   = $container->get('settings');
        $loader     = new Illuminate\Translation\FileLoader(new Illuminate\Filesystem\Filesystem(), __DIR__ . '/translations');
        $translator = new Illuminate\Translation\Translator($loader, $_SESSION['locale'] ?? $settings['defaultLocale']);
        $translator->setFallback($settings['defaultLocale']);
        return $translator;
    };

    // container set $GLOBALS variable
    $GLOBALS['container'] = $container;

?>